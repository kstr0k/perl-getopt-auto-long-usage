# Getopt::Auto::Long::Usage

## _Auto-help from Getopt::Long specs_

This module generates simple usage / help messages by parsing [Getopt::Long](https://metacpan.org/pod/Getopt%3A%3ALong) argument specs (and optionally using provided descriptions). There are also functions for ferrying parsed options from perl to bash.

See the [CPAN](https://metacpan.org/pod/Getopt::Auto::Long::Usage) project page. Also, `perldoc Getopt::Auto::Long::Usage` once installed (e.g. via `cpanm`).

## Usage

Here's a standard usage subroutine that prints to stdout if help is requested, or stderr on error:

```
use Getopt::Auto::Long::Usage;
use Getopt::Long;
my @getoptargs = qw{ help delim:s eval|e!  };
my %_O_; my @getoptconf = (\%_O_, @getoptargs);

sub usage {
  my ($rc) = @_;
  my @dsc = ( delim => 'instead of newline' );
  print getoptlong2usage(
    Getopt_Long => \@getoptconf, # all others optional
    descriptions => \@dsc
  );
  exit $rc if defined( $rc );
}

Getopt::Long::Configure( qw(
  no_ignore_case no_auto_abbrev no_getopt_compat
  gnu_compat bundling
));
unless( GetOptions( @getoptconf ) ) {
  local *STDOUT = *STDERR; usage 1;
}
usage 0 if( $_O_{ help } );
```

## Copyright

`Alin Mr. <almr.oss@outlook.com>` / MIT license

# SEE ALSO

- [bashaaparse](https://gitlab.com/kstr0k/bashaaparse/), my take on automatic arg parsing / usage generation for Bash scripts

